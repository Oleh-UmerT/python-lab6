import os
import shutil
import time
import csv

print("Task1-----------------------------")

f = open("numbers.txt", "w")
z = 0
arr1 = []
while z < 10:
    num = int(input(f"Введіть число {z+1} "))
    if z < 9:
        f.write(f"{str(num)}\n")
    else:
        f.write(str(num))
    z += 1
f.close()
f = open("numbers.txt", "r")
inf = f.read()
f.close()
x = inf.split("\n")
list1 = []
for a in x:
    list1.append(int(a))
res = sum(list1)
print(res)
f = open("sum_numbers.txt", "w")
f.write(str(res))
f.close()

print("Task2-----------------------------")

f = open("numbers2.txt", "w")
z1 = int(input("Число 0 завершує ввод чисел "))
while z1 != 0:
    if z1 % 2 == 0:
        f.write(f"{z1} - парне число\n")
    else:
        f.write(f"{z1} - непарне число\n")
    z1 = int(input("Число 0 завершує ввод чисел "))
f.close()

print("Task3-----------------------------")

arr3 = []
newArr = []
with open('learning_python.txt') as file:
    for i in file:
        i = i.strip()
        arr3.append(i)
newArr.extend(sorted(arr3))
print(newArr)

print("Task4-----------------------------")


shutil.copy(r"learning_python.txt", r"task4")

arr4 = []
with open(r'task4\learning_python.txt') as file:
    for i in file:
        i = i.strip()
        i = i.replace("Python", "C++")
        arr4.append(i)
print(arr4)
file.close()
fT = open(r"task4\newFile.txt", "w")
fF = open(r"task4\newFileFalse.txt", "w")
for j in arr4:
    print(j)
    g = str(input("Чи є твердження вірним?(y/n)  "))
    if g == "y":
        fT.write(f"{j}\n")
    elif g == 'n':
        fF.write(f"{j}\n")
os.remove(r"task4\learning_python.txt")
fT.close()
fF.close()
os.rename(r"task4\newFile.txt", r"task4\learning_python.txt")

print("Task5-----------------------------")

g = time.ctime(os.path.getmtime("guest_book.txt"))
h = time.ctime(os.path.getctime("guest_book.txt"))
print(g)
f = open("guest_book.txt", "a")
f.write(f"Файл створено - {h}\n")
f.write(f"Данні внесено - {g}\n")
f.close()
f = open("guest_book.txt", "a")
name = str(input("Введіть ваше ім'я  "))
print(f"Доброго дня, {name}!")
f.write(f"Доброго дня, {name}!\n\n")

print("Task6-----------------------------")

def letters():
    timeS = time.time()
    f = open("randomText.txt", "r").read().lower()
    d = {}
    n = 0
    for letter in f:
        if ord('a') <= ord(letter) <= ord('z'):
            x = d.get(letter, 0)
            d[letter] = x + 1
            n += 1
    count = [(i, "{:f}".format(d[i] / n)) for i in d.keys()]
    timeF = time.time()
    resTime = timeF - timeS
    return count, resTime


def words():
    timeS = time.time()
    f = open("randomText.txt", "r").read().lower()
    d = {}
    n = 0
    for word in f.split():
        d[word] = d.get(word, 0) + 1
        n += 1
    count = [(i, "{:f}".format(d[i] / n)) for i in d.keys()]
    timeF = time.time()
    resTime = timeF - timeS
    return count, resTime


count, resTime = words()

result = "\n".join([i[0] + " " + i[1] for i in count])
print(result)
with open("resultT6.txt", "w", newline='\n') as file:
    print('Файл створено - ' + time.ctime(os.path.getctime('randomText.txt')), file=file)
    print(result, file=file)
    print('Час пошуку - ' + "{:f}.c".format(resTime), file=file)
    print('Файл змінено - ' + time.ctime(os.path.getmtime('randomText.txt')), file=file)

print("Task7-----------------------------")

count = 0
mark = 0


def count_marks(l):
    arr7 = []
    stroka = input("Уведіть оцінку - ")
    for i in range(len(l)):
        arr7.append(l[i][4])
    print(f"Кількість учнів щo отримали оцінку {stroka} - " + str(arr7.count(stroka)))


with open('marks.lab6.csv', 'rt', encoding='UTF-8') as freading:
    creating = csv.reader(freading)
    listMarks = [row for row in creating]
count_marks(listMarks)
file = open("answer_about_mark.txt", 'a+')
for i in range(0, 21):
    for line in listMarks:
        if int(line[3][:2]) == i:
            count += 1
            mark += float(line[4].replace(",", "."))
    if count != 0:
        file.write(f"За {i} хвилин {count} чоловік, отримало {round(mark / count, 2)} балів\n")
    else:
        file.write(f"За {i} хвилин ніхто не закінчив тест\n")
file.close()
questions = 0
f = open("percent.txt", 'a+')
for i in range(5, 25):
    cTrue = 0
    questions += 1
    for j in listMarks:
        if j[i] == '0,50':
            cTrue += 1
    answerT = (cTrue / count) * 100
    answerF = 100 - answerT
    f.write(f"{answerT}% правильних та {answerF}% не правильних в питанні {questions}\n")
f.close()
for i in range(len(listMarks)):
    for j in range(len(listMarks)):
        if float(listMarks[i][4].replace(",", ".")) < float(listMarks[j][4].replace(",", ".")):
            temp = listMarks[i]
            listMarks[i] = listMarks[j]
            listMarks[j] = temp
for i in range(len(listMarks)):
    for j in range(len(listMarks)):
        if float((listMarks[i][3][:2]).strip() + "." + listMarks[i][3][:-6][:2]) < float(listMarks[j][3][:2].strip() + "." + listMarks[j][3][:-6][:2]) and not float(listMarks[i][4].replace(",", ".")) < float(listMarks[j][4].replace(",", ".")):
            temp = listMarks[i]
            listMarks[i] = listMarks[j]
            listMarks[j] = temp
file = open('percent.txt', 'a+', encoding="UTF-8")
print(f"5 кращих оцінка/час = \n{listMarks[0]}\n,{listMarks[1]}\n,{listMarks[2]}\n,{listMarks[3]}\n,{listMarks[4]}")
file.write(f"5 кращих оцінка/час = \n{listMarks[0]}\n, {listMarks[1]}\n, {listMarks[2]}\n,{listMarks[3]}\n,{listMarks[4]}\n")
file.close()
